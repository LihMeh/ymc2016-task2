package com.myarchway.mikhail.melody;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.os.PowerManager;
import android.widget.Toast;

import static android.media.AudioManager.AUDIOFOCUS_GAIN;
import static android.media.AudioManager.AUDIOFOCUS_LOSS;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT;
import static android.media.AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK;

/*
  foreground сервис, воспроизводящий аудио поток по url.
  Использует свойство streamUrl в SharedPreferences под название MelodyPrefs.
  Для использования сначала делаем startService, потом bindService! (иначе сервис может остановиться после выхода из activity)
 */
public final class PlayService extends Service implements MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener,AudioManager.OnAudioFocusChangeListener {

    private static final String DEFAULT_URL = "http://stream3.radiostyle.ru:8001/biker-fm";

    private String streamUrl;

    private int startId = 0;

    @Override
    /*
    Вызывается, если не позучилось создать поток.
     */
    // TODO: На самом деле, этот callback может и не вызываться. Проблема в том, что я создаю MediaPlayer в потоке AsyncTask'а... а в этом случае данный callback может не работать. Выходит, пользователь видит сообщение, если не получилось откирыть поток в самом начале,   но не получает оповещения, если интернет обрубился.
    public boolean onError(MediaPlayer mp, int what, int extra) {
        sendToast(R.string.error_cant_connect);
        return false;   // возвращаем false, чтобы вызвался onCompletion
    }

    @Override
    /*
     Использую этот callback для того, чтобы ставить завершенный статус для сервиса, когда воспроизведение музыки прекратилось. В отличие от onError,  этот callback срабатывает всегад и корректно.
    */
    public void onCompletion(MediaPlayer mp) {
        setState(State.Stopped);
    }


    /*
    Приостанавливает воспроизведение при звонке, оповещениях и т.д.
     */
    @Override
    public void onAudioFocusChange(int focusChange) {
        switch (focusChange) {
            case AUDIOFOCUS_GAIN:
                if((currentState==State.Paused)&&lostFocusButWannaResume) {
                    lostFocusButWannaResume = false;
                    start();
                }
                break;
            case AUDIOFOCUS_LOSS:
                lostFocusButWannaResume = false;
                stop();
                break;
            case AUDIOFOCUS_LOSS_TRANSIENT:
            case AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK:
                if (currentState!=State.Playing) break;
                pause();
                lostFocusButWannaResume = true;
                break;
        }
    }
    private boolean lostFocusButWannaResume = false;    // используется медотом onAudioFocusChange

    // Текущее состояние сервиса.
    private State currentState = State.Stopped;
    enum State {
        Stopped,
        Starting,
        Playing,
        Paused
    }

    private MediaPlayer player = null;

    private ServiceStateChangedListener stateChangedListener = null;

    public PlayService() {

    }

    /*
    Используется, чтобы установить callback для управляющей activity.
    Однако, хранит только один текущий listener!
    Удаляется от  вызовом этого метода с null.
    Это упрощение / упущение сделано потому, что у нас тут всего одни клиент. Но вообще, это неправильно.
     */
    public void setStateChangedListener(ServiceStateChangedListener listener) {
        this.stateChangedListener = listener;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        streamUrl = getApplicationContext().getSharedPreferences("MelodyPrefs", 0).getString("streamUrl",DEFAULT_URL);
    }

    @Override
    public void onDestroy() {
        if (player!=null) player.release();
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);
        return START_STICKY;
    }

    /*
    private Notification.Builder notificationBuilder = null;
    private Notification buildNotification(int messageId) {
        if (notificationBuilder==null) {
            notificationBuilder = new Notification.Builder(getApplicationContext());
            notificationBuilder.setSmallIcon(R.drawable.service_notification_icon);
            notificationBuilder.setContentTitle(getString(R.string.service_notification_title));
            Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,notifyIntent,0);
            notificationBuilder.setContentIntent(pendingIntent);

        }
        notificationBuilder.setContentText(getString(messageId));
        return notificationBuilder.build();
    }
    */
    private Notification buildNotification(int messageId) {
        Notification.Builder notificationBuilder = new Notification.Builder(getApplicationContext());
        notificationBuilder.setSmallIcon(R.drawable.service_notification_icon);
        //notificationBuilder.setLargeIcon(R.drawable.service_notification_icon);
        notificationBuilder.setContentTitle(getString(R.string.service_notification_title));
        Intent notifyIntent = new Intent(getApplicationContext(), MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(),0,notifyIntent,0);
        notificationBuilder.setContentIntent(pendingIntent);
        notificationBuilder.setContentText(getString(messageId));
        return notificationBuilder.build();
    }
    // Сначала я сделал buildNotification так, как тут есть..
    // Потом я подумал, а не кэшировать ли notificationBuilder...
    // Раз так, то надо бы кешировать еще и и PendingIntent...
    // а потом задумался... а не приводит ли это к утечке?
    // TODO: Разобраться, можно ли использовать один PendingIntent множество раз.



    // Текущее состояние сервиса.
    public State getState() { return currentState;}

    /*
    Запустить воспроизведение. Метод можно вызывать, не проверяя состояние предварительно.
     */
    public void start() {
        switch (currentState){
            case Stopped:
                (new StartPlaybackTask()).execute(streamUrl);
                setState(State.Starting);
                break;
            case Paused:
                if (player!=null) {
                    player.start();
                    setState(currentState = State.Playing);
                }
                break;
            case Playing:
                // nop
                break;
        }

    }

    /*
    Поставить воспроизведение на паузу (и начать кеширование). Метод можно вызывать, не проверяя состояние предварительно.
     */
    public void pause() {
        if (currentState!=State.Playing) return;
        player.pause();
        setState(State.Paused);
    }

    /*
    Остановить воспроизведение. Ресурсы будут отчищены. (а оповещение уберется). Метод можно вызывать, не проверяя состояние предварительно.
     */
    public void stop() {
        if ((currentState!=State.Playing)&&(currentState!=State.Paused)) return;
        setState(currentState = State.Stopped);
        player.stop();
        player.release();
        player = null;
        stopForeground(true);
    }



    public class LocalBinder extends Binder {

        public PlayService getService() {
            return PlayService.this;
        }

    }

    private void setState(State newState) {
        State oldState = this.currentState;
        this.currentState = newState;

        if (oldState!=newState) {
            stopForeground(true);
            if (newState != State.Stopped) {
                Notification notification = buildNotification(getStatusStringId());
                startId++;                                          // На Nexus 4 все прекрасно работало с постоянным ненулевым startId. На Galaxy S4 mini с Android 4.4, оповещение не обновлялось, поэтому id каждый раз новый.
                startForeground(startId, notification);
            }
        }

        if (stateChangedListener!=null) stateChangedListener.serviceStateChanged(newState);
    }

    /*
    Асинхронное задание для запуска воспроизведения.
    Это нельзя делать в основном потоке, инача интерфейс подвиснет.
    Вообще, надо было делать player.setDataSource  и player.prepareAsync,  но я уже сделал так  и оно работает :)
     */
    private class StartPlaybackTask extends AsyncTask<String,Object,Boolean> {

        @Override
        protected Boolean doInBackground(String... params) {
            if (!requestAudioFocus()) return false;
            Uri uri = Uri.parse(params[0]);
            player = MediaPlayer.create(getApplicationContext(), uri);
            if (player==null) return false;
            player.setOnErrorListener(PlayService.this);
            player.setOnCompletionListener(PlayService.this);
            player.setWakeMode(getApplicationContext(), PowerManager.PARTIAL_WAKE_LOCK);
            player.start();
            return player.isPlaying();
        }

        @Override
        protected void onPostExecute(Boolean started) {
            if (started) {
                setState(State.Playing);
            }
            else {
                onError(null,0,0);
                onCompletion(null);
                //setState(State.Stopped);
            }
            super.onPostExecute(started);
        }
    }

    private boolean requestAudioFocus() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int result = audioManager.requestAudioFocus(this, AudioManager.STREAM_MUSIC,
                AUDIOFOCUS_GAIN);
        return (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED);
    }


    public interface ServiceStateChangedListener {
        void serviceStateChanged(PlayService.State newState);
    }


    /*
    Получить id ресурса со строкой, описывающей текущее состояние плеера. Эта же строка пишется в оповещении.
     */
    public int getStatusStringId() {
        // Вообще, тут бы лучше сделать Map<State,Int> ... код этого метода станет красивее, но  в другом месте его придется заполнять.  Имеет ли смысл?
        switch(currentState) {
            case Stopped:
                return R.string.status_stopped;
            case Starting:
                return R.string.status_starting;
            case Paused:
                return R.string.status_paused;
            case Playing:
                return R.string.status_playing;
            default:
                return R.string.status_unknown;
        }
    }

    private void sendToast(int stringId) {
        Toast toast = Toast.makeText(getApplicationContext(),stringId,Toast.LENGTH_LONG);
        toast.show();
    }

}
