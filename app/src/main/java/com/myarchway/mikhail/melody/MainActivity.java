package com.myarchway.mikhail.melody;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class MainActivity extends Activity implements ServiceConnection, PlayService.ServiceStateChangedListener {

    // binder для нашего сервиса. Обновляется после onResume  и очищается при onPause.
    private PlayService.LocalBinder serviceBinder = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fillStateMap();
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateView();
        Intent intent = new Intent(this, PlayService.class);
        startService(intent);
        intent = new Intent(this, PlayService.class);
        bindService(intent,this, Context.BIND_AUTO_CREATE); // В принципе, auto_create - бесполезное значение в данном случае... мы строчкой раньше создали сервис.
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (serviceBinder!=null) {
            serviceBinder.getService().setStateChangedListener(null);
            unbindService(this);
        }
    }


    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        serviceBinder = (PlayService.LocalBinder)service;
        serviceBinder.getService().setStateChangedListener(this);
        updateView();
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        serviceBinder = null;
    }

    // В этом списке для каждого из возможных состояний сервиса мы храним список кнопок, которые должны быть активны.
    private Map<PlayService.State,int[]> stateMap= new HashMap<PlayService.State,int[]>();
    private void fillStateMap() {
        stateMap.put(PlayService.State.Stopped,new int[]{R.id.btnStart});
        stateMap.put(PlayService.State.Starting,new int[]{});
        stateMap.put(PlayService.State.Playing,new int[]{R.id.btnStop, R.id.btnPause});
        stateMap.put(PlayService.State.Paused,new int[]{R.id.btnStart, R.id.btnStop});
    }

    private void updateView() {
        // А вот тут пользуемся картой состояний. Сначала делаем все кнопки неактивными,  а потом включаем те, которые нужны.
        for (int btn: new int[]{R.id.btnStop,R.id.btnStart, R.id.btnPause}) {
            ((Button)findViewById(btn)).setEnabled(false);
        }
        if (serviceBinder==null) return;
        PlayService.State serviceState = serviceBinder.getService().getState();
        for (int btn: stateMap.get(serviceState) ) {
            ((Button)findViewById(btn)).setEnabled(true);
        }
        ((TextView)findViewById(R.id.statusTextView)).setText(getText(serviceBinder.getService().getStatusStringId()));
    }

    public void OnStartBtnClick(View btn) {
        if (serviceBinder==null) return;
        serviceBinder.getService().start();
    }

    public void OnPauseBtnClick(View btn) {
        if (serviceBinder==null) return;
        serviceBinder.getService().pause();
    }

    public void OnStopBtnClick(View btn) {
        if (serviceBinder==null) return;
        serviceBinder.getService().stop();
    }

    @Override
    public void serviceStateChanged(PlayService.State newState) {
        updateView();
    }
}
