package com.myarchway.mikhail.melody;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * Instrumentation service_notification_icon, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under service_notification_icon.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("com.myarchway.mikhail.melody", appContext.getPackageName());
    }
}
